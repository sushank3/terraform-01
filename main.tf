provider "aws" {
    region = "ap-south-1"
}


variable "vpc_cidr_block" {}
variable "subnet_cidr_block" {}
variable "avail_zone" {}
variable "env_prefix"{}
variable "my-ip" {}
variable "instance_type" {}
variable "my_pub_key_location" {}
# variable "script_location" {}


resource "aws_vpc" "myapp-vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
        Name: "${var.env_prefix}-vpc"
    }
}

resource "aws_subnet" "myapp-subnet-1" {
    vpc_id = aws_vpc.myapp-vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.avail_zone
    tags = {
        Name: "${var.env_prefix}-subnet-1"
    }
}

resource "aws_route_table" "myapp-route-table" {
    vpc_id = aws_vpc.myapp-vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.myapp-igw.id
    }

    tags = {
        Name: "${var.env_prefix}-rtb"
    }
}

resource "aws_route_table_association" "a-rtb-subnet" {
    subnet_id = aws_subnet.myapp-subnet-1.id
    route_table_id = aws_route_table.myapp-route-table.id
  
}

resource "aws_internet_gateway" "myapp-igw" {
    vpc_id = aws_vpc.myapp-vpc.id
    tags = {
      "Name" = "${var.env_prefix}-igw"
    }
}

resource "aws_security_group" "myapp-sg" {
    name = "myapp-sg"
    vpc_id = aws_vpc.myapp-vpc.id

    #ingress for incoming traffic
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.my-ip]
    }

     ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 50000
        to_port = 50000
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    #egress for outgoing traffic
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        prefix_list_ids = []
    }

    tags = {
      "Name" : "${var.env_prefix}-sg"
    }
  
}

#To fetch data from AWS we use data type
# data "aws_ami" "latest-amazon-linux-image" {
#     # most_recent = true
#     # owners = ["amazon"]
#     # filter {
#     #   name = "name"
#     #   values = ["ami-0fb653ca2d3203ac1"]
#     # }

#     filter {
#       name = "virtualization-type"
#       values = ["hvm"]
#     }
  
# }

# output "aws_ami_id" {
#     value = data.aws_ami.latest-amazon-linux-image.id
# }

output "aws_public_id" {
    value = aws_instance.myapp-server.public_ip
  
}


resource "aws_key_pair" "ssh-key" {
    key_name = "tf-server"
    public_key = file(var.my_pub_key_location)
  
}

resource "aws_instance" "myapp-server" {
    # ami = data.aws_ami.latest-amazon-linux-image.id
    ami = "ami-0851b76e8b1bce90b"
    instance_type = var.instance_type

    subnet_id = aws_subnet.myapp-subnet-1.id
    vpc_security_group_ids = [aws_security_group.myapp-sg.id]
    availability_zone = var.avail_zone

    associate_public_ip_address = true
    key_name = aws_key_pair.ssh-key.key_name

    # user_data = <<-EOF
    #             #!/bin/bash
    #             echo "Hello, World" 
    #             EOF

    user_data = file("entry_script.sh")

    user_data_replace_on_change = true

    /*server entrypoint script, this bash script
      runs after the succesfull creation
      
      The commands will run only for the first time
      */

    # user_data = <<-EOF
    #             #!/bin/bash
                
    #             sudo apt update -y && sudo apt install -y docker
    #             sudo systemctl start docker
    #             sudo usermod -aG docker ec2-user
    #             docker run -p 8080:80 nginx

    #             echo "Hello, World" > index.html
    #             nohup busybox httpd -f -p 8080 &
    #             EOF

    # user_data_replace_on_change = true
    # user_data = file(var.script_location)

    

    tags = {
      Name = "${var.env_prefix}-server"
    }

}

