#!/bin/bash
apt update -y && apt install -y docker.io
systemctl start docker
sudo usermod -aG docker ubuntu
docker run -p 50000:80 nginx

# echo "Hello, World" > index.html
# nohup busybox httpd -f -p 8080 &
